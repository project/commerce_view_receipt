<?php

namespace Drupal\commerce_view_receipt\Plugin\Menu\LocalAction;

use Drupal\Core\Menu\LocalActionDefault;
use Drupal\Core\Routing\RouteMatchInterface;

class ViewPdfAction extends LocalActionDefault {

  /**
   * {@inheritdoc}
   */
  public function getRouteParameters(RouteMatchInterface $route_match) {
    return [
      'entity_type' => 'commerce_order',
      'export_type' => 'pdf',
      'entity_id' => $route_match->getParameter('commerce_order')->id()
    ];
  }

}

