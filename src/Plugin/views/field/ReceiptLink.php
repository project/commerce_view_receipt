<?php declare(strict_types = 1);

namespace Drupal\commerce_view_receipt\Plugin\views\field;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Url;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Provides Receipt link field handler.
 *
 * @ViewsField("commerce_view_receipt_receipt_link")
 */
final class ReceiptLink extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function query(): void {
    // For non-existent columns (i.e. computed fields) this method must be
    // empty.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values): string|array|MarkupInterface {
    /** @var \Drupal\commerce_order\Entity\Order $order */
    $order = $this->getEntity($values);

    if ($order->getState()->getId() === 'completed') {
      $url = Url::fromRoute('commerce_view_receipt', [
        'commerce_order' => $order->id(),
      ]);

      if (!$url->access()) {
        return '';
      }

      return [
        '#type' => 'link',
        '#url' => $url,
        '#title' => $this->t('Receipt'),
      ];
    }

    return '';
  }

}
