<?php

namespace Drupal\commerce_view_receipt\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;

class ViewPdfDeriver extends DeriverBase {

  /**
   * {@inheritdoc}
   */ 
  public function getDerivativeDefinitions($base_plugin_definition) {
    // Only show if entity_print module is enabled.
    if (\Drupal::moduleHandler()->moduleExists('entity_print')) {
      $this->derivatives['commerce_view_receipt.view_pdf'] = $base_plugin_definition;
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}

