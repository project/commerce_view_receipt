# Commerce View Receipt

This module adds a "Receipt" tab to admin commerce order pages.  The tab
renders the order using the commerce order receipt template that is emailed
to customers.  Very useful when customizing the receipt template -- view your
changes without needing to send an email.

- For a full description of the module, visit the
  [project page](https://www.drupal.org/project/commerce_view_receipt).

- To submit bug reports or feature suggestions, or track changes
  [issue queue](https://www.drupal.org/project/issues/commerce_view_receipt).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

- [Commerce](https://www.drupal.org/project/commerce)


## Installation

Install as you would normally install a contributed Drupal module. Visit
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules)
for further information.


## Configuration

Enable the module.  Grant the 'view receipts' permission where appropriate.


## Maintainers

- Greg Mack - [gregcube](https://www.drupal.org/u/gregcube)
